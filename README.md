
Case study files (checked in)

1. Data cleaning python script
2. Postgres queries for analysis

Case study links (not checked in)

1. Link to process documentation (not checked in, but useful): https://lucid.app/lucidchart/7344d495-e221-4a2d-91e7-731b263d5c03/edit?viewport_loc=-1514%2C-164%2C2673%2C1293%2C0_0&invitationId=inv_8cfa9ac5-6344-4a42-8962-9a5d3dfadcdf
2. Link to presentation: https://docs.google.com/presentation/d/1sgq6M2cBZGVueLcLPU2I24hmRBTw3BOSs0ikYzOTQp0/edit#slide=id.g2b4738b2758_0_148
