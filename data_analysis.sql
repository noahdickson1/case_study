-- AOV figures

with orders_agg as (
	select
		-- Online AOV
		sum(
			case
				when not source_name = 'Point of Sale' and not order_type = 'Service'
					then net_sales
				else 0
			end
		) net_sales_online,
		count(distinct
			case
				when not source_name = 'Point of Sale' and not order_type = 'Service'
				  then surrogate_key
				else null
			end
		) as number_of_orders_online,
		
		-- Retail AOV
		sum(
			case
				when source_name = 'Point of Sale' and not order_type = 'Service'
					then net_sales
				else 0
			end
		) as net_sales_retail,
		count(distinct
			case
				when source_name = 'Point of Sale' and not order_type = 'Service'
				  then surrogate_key
				else null
			end
		) as number_of_orders_retail,

		-- Total AOV
		sum(net_sales) as net_sales,
		count(distinct surrogate_key) as number_of_orders,
	
		-- Online ARPU
		sum(
			case
				when not source_name = 'Point of Sale' and shopify_customer_id is not null and not order_type = 'Service'
					then net_sales
				else 0
			end
		) as net_sales_customer_online,
		count(distinct
			case
				when not source_name = 'Point of Sale' and shopify_customer_id is not null and not order_type = 'Service'
				  then shopify_customer_id
				else null
			 end
		) as number_of_customers_online,
		
		-- Retail ARPU
		sum(
			case
				when source_name = 'Point of Sale' and shopify_customer_id is not null and not order_type = 'Service'
					then net_sales
				else 0
			end
		) as net_sales_customer_retail,
		count(distinct
			case
				when source_name = 'Point of Sale' and shopify_customer_id is not null and not order_type = 'Service'
				  then shopify_customer_id
				else null
			end
		) as number_of_customers_retail,

		-- Total ARPU
		sum(
			case
				when shopify_customer_id is not null
					then net_sales
				else 0
			end
		) as net_sales_customer,
		count(distinct shopify_customer_id) as number_of_customers
	from case_study.sales
)

select *
from orders_agg

;

-- Sales by Location

with orders_agg as (
	select
		location_id,
		count(distinct surrogate_key) as number_of_orders,
		sum(net_sales) as total_net_sales
	from case_study.sales
	group by location_id
)

select *
from orders_agg
order by total_net_sales desc

;

-- Days each location has been open

select
    location_id,
    min(created_at) as first_sale_date,
    max(created_at) as last_sale_date,
    EXTRACT(DAY FROM max(created_at) - min(created_at)) as days_open
from case_study.sales
group by location_id

;

-- Traffic Conversion Rates

with traffic_totals as (
	select
        location_id,
        sum(traffic_out) as total_traffic
	from case_study.foot_traffic
	group by location_id
	order by total_traffic desc
),

sales_totals as (
	select
		sales.location_id,
		count(distinct surrogate_key) as number_of_orders,
		sum(net_sales) as total_net_sales
	from case_study.sales
	inner join case_study.foot_traffic
		on foot_traffic.date_time = sales.created_at
		and foot_traffic.location_id::float = sales.location_id::float
	group by sales.location_id
)

select
    traffic_totals.location_id,
    traffic_totals.total_traffic,
    sales_totals.number_of_orders,
    sales_totals.total_net_sales
from sales_totals
left join traffic_totals
	on traffic_totals.location_id::float = sales_totals.location_id::float
