import pandas as pd
from sqlalchemy import create_engine


postgres_username = ""
postgres_password = ""
postgres_schema = "case_study"
engine = create_engine(f"postgresql://{postgres_username}:{postgres_password}@localhost:5433/{postgres_schema}")
connection = engine.raw_connection()
cursor = connection.cursor()

appointments_file_path = "Case Study Data/appointments.csv"
foot_traffic_file_path = "Case Study Data/foot_traffic.csv"
sales_file_path = "Case Study Data/sales.csv"


# The general approach in each of the following 3 functions is:
# 1. Import data from CSV
# 2. Clean column names and deduplicate
# 3. Write data to postgres table

def clean_appointment_data_and_create_table():
    appointments_df = pd.read_csv(appointments_file_path, header=0, parse_dates=["BOOK_DATE", "APPOINTMENT_DATE"], dtype={
        "ACUITY_CUSTOMER_ID": str,
        "EMAIL_HASH": str,
        "APPOINTMENT_ID": str,
        "SERVICE": str,
        "LOCATION_ID": str,
        "CANCELED": str,
        "NO_SHOW": str,
        "LABEL": str,
    })
    appointments_df.columns = appointments_df.columns.str.strip().str.upper()
    appointments_df = appointments_df.dropna(subset=["APPOINTMENT_ID"]).drop_duplicates()
    appointments_df = appointments_df.sort_values(["LOCATION_ID", "APPOINTMENT_DATE"], ascending=[False, False], na_position="last").reset_index(drop=True)
    appointments_df = appointments_df.drop_duplicates(["APPOINTMENT_ID"])

    appointments_df.columns = appointments_df.columns.str.lower()
    appointment_rows = appointments_df.to_sql("appointments", engine, schema="case_study", if_exists="replace")
    cursor.execute(f"SELECT count(*) FROM case_study.appointments")
    appointment_rows = cursor.fetchone()
    print(f"Number of rows inserted into appointments table: {appointment_rows}")


def clean_foot_traffic_data_and_create_table():
    foot_traffic_df = pd.read_csv(foot_traffic_file_path, header=0, parse_dates=["DATE_TIME"], dtype={
        "START_TIME": str,
        "END_TIME": str,
        "TRAFFIC_OUT": int,
        "LOCATION_ID": str,
    })
    foot_traffic_df.columns = foot_traffic_df.columns.str.strip().str.upper()
    foot_traffic_df = foot_traffic_df.dropna(subset=["LOCATION_ID"]).drop_duplicates()
    foot_traffic_df["TIMESTAMP"] = pd.to_datetime(foot_traffic_df["DATE_TIME"].astype(str) + " " + foot_traffic_df["START_TIME"])
    foot_traffic_df = foot_traffic_df.sort_values(["LOCATION_ID", "TIMESTAMP"], ascending=[False, False], na_position="last").reset_index(drop=True)

    foot_traffic_df.columns = foot_traffic_df.columns.str.lower()
    foot_traffic_df.to_sql("foot_traffic", engine, schema="case_study", if_exists="replace")
    cursor.execute(f"SELECT count(*) FROM case_study.foot_traffic")
    foot_traffic_rows = cursor.fetchone()
    print(f"Number of rows inserted into foot_traffic table: {foot_traffic_rows}")


def clean_sales_data_and_create_table():
    sales_df = pd.read_csv(sales_file_path, header=0, parse_dates=["created_at"], dtype={
        "sale_id": str,
        "alt_order_id": str,
        "order_id": str,
        "shopify_order_name": str,
        "refund_id": str,
        "sale_kind": str,
        "shopify_customer_id": str,
        "location_id": str,
        "gateway": str,
        "source_name": str,
        "shipping_state": str,
        "shipping_country": str,
        "customer_order": str,
        "order_tags": str,
        "canceled_at": str,
        "order_type": str,
        "gross_sales": float,
        "discounts": float,
        "returns": float,
        "net_sales": float,
        "shipping": float,
        "tax": float,
        "gross_units": float,
        "net_units": float,
    })
    sales_df.columns = sales_df.columns.str.strip().str.upper()
    sales_df = sales_df.drop_duplicates().dropna(subset=["NET_SALES"]).reset_index(drop=True)
    sales_df = sales_df.sort_values(["CREATED_AT", "ALT_ORDER_ID"], ascending=[False, False], na_position="last").reset_index(drop=True)
    sales_df["ROW_NUMBER"] = sales_df.index
    sales_df["SURROGATE_KEY"] = sales_df.SALE_ID.combine_first(sales_df.ROW_NUMBER)
    sales_df = sales_df.drop_duplicates(["SURROGATE_KEY"])

    sales_df.columns = sales_df.columns.str.lower()
    sales_df.to_sql("sales", engine, schema="case_study", if_exists="replace")
    cursor.execute(f"SELECT count(*) FROM case_study.sales")
    sales_rows = cursor.fetchone()
    print(f"Number of rows inserted into sales table: {sales_rows}")


clean_appointment_data_and_create_table()
clean_foot_traffic_data_and_create_table()
clean_sales_data_and_create_table()

cursor.close()
